package fr.systao.experiments.profiler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.systao.experiments.profiler.service.GreetingsService;

@RestController
@RequestMapping(path="/greetings")
public class GreetingsController {
	
	@Autowired
	private GreetingsService service;
	
	@GetMapping(path="/{duration}")
	public String greet(@PathVariable("duration") int duration) throws Exception {
		return service.greet(duration);
	}
}
