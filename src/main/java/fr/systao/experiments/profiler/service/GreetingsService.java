package fr.systao.experiments.profiler.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.systao.experiments.profiler.aspects.Enable;
import fr.systao.experiments.profiler.aspects.Level;
import fr.systao.experiments.profiler.aspects.Profiled;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GreetingsService {
	@Profiled(value="greet", level=Level.INFO, args=false)
	public String greet(int duration) throws Exception {
		this.pause(duration);
		this.notProfiled();
		return "Greetings!";
	}
	
	private void notProfiled() {
		log.info("not profiled");
	}

	@Profiled(value="pausing thread execution", level=Level.DEBUG)
	private void pause(int duration) throws Exception {
		Thread.sleep(duration);
	}

}
