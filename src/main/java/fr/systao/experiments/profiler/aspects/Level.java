package fr.systao.experiments.profiler.aspects;

import java.util.function.BiConsumer;
import java.util.function.Function;

import org.slf4j.Logger;

public enum Level{

    TRACE(l -> l::trace, Logger::isTraceEnabled),
    DEBUG(l -> l::debug, Logger::isDebugEnabled),
    INFO (l -> l::info, Logger::isInfoEnabled),
    WARN (l -> l::warn, Logger::isWarnEnabled),
    ERROR(l -> l::error, Logger::isErrorEnabled);

	private final Function<Logger, BiConsumer<String, Object[]>> log;
	private final Function<Logger, Boolean> enabled;

	Level(Function<Logger, BiConsumer<String, Object[]>> log, Function<Logger, Boolean> enabled) {
		this.log = log;
		this.enabled = enabled;
	}
	
    void log(Logger logger, String message, Object...params) {
    	if ( isEnabled(logger) ) {
    		log.apply(logger).accept(message, params);
    	}
    }
    
    boolean isEnabled(Logger logger) {
    	return enabled.apply(logger);
    }
}