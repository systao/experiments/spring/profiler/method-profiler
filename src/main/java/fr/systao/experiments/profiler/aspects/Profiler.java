package fr.systao.experiments.profiler.aspects;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

@Aspect
public class Profiler {
	
	@Pointcut("@annotation(fr.systao.experiments.profiler.aspects.Profiled)")	
	public void profiledMethod() { }
	
	@Pointcut("execution(* fr.systao.experiments..*(..))")
	public void anyMethods() { }
	
	@Pointcut("if()")
	public static boolean enabled() { 
		return Enable.getInstance().isEnabled();
	}
	
	@Around("enabled() && profiledMethod() && anyMethods()")
	public Object profile(ProceedingJoinPoint joinPoint) throws Throwable {
		StopWatch stopwatch = new StopWatch();
		Class<?> type = joinPoint.getTarget().getClass();
		Logger log = LoggerFactory.getLogger(String.join(".", "metrics", type.getName()));
		try {
			stopwatch.start();
			return joinPoint.proceed();
		}
		finally {
			stopwatch.stop();
			Profiled config = getConfig(joinPoint);
			Level level = config.level();
			level.log(log, 
				"{}. Duration in ms: {}. Parameters: {}", 
				config.value(), 
				stopwatch.getTotalTimeMillis(),
				Arrays.asList(joinPoint.getArgs()));
		}
	}
	
	private Profiled getConfig(ProceedingJoinPoint joinPoint) throws Exception {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String methodName = signature.getMethod().getName();
		Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
		Class<?> targetClass = joinPoint.getTarget().getClass();
		Method method = targetClass.getDeclaredMethod(methodName,parameterTypes);
		return method.getAnnotation(Profiled.class);
	}
}
