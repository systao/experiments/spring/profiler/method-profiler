package fr.systao.experiments.profiler.aspects;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_SINGLETON;

@Getter
@Setter
@Component
@Scope(SCOPE_SINGLETON)
@ConfigurationProperties(prefix="profiler")
public class Enable {
	@Getter
	private static Enable instance;
	
	private boolean enabled;
	
	public Enable() {
		instance = this;
	}
}
