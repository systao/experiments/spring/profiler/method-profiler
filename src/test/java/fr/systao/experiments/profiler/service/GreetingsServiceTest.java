package fr.systao.experiments.profiler.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GreetingsServiceTest {

	@Autowired
	private GreetingsService service;
	
	@Test
	public void testGreet() throws Exception {
		service.greet(50);
	}

}
