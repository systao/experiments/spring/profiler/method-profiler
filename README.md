# Full AspectJ support with Spring Boot

## Spring AOP limitations

Spring AOP may be enough for most use-cases but still falls short in some areas, in particular:

- Spring AOP can only instruments components loaded in the application context
- Spring AOP doesn't support the full range of AspectJ pointcut expressions
- Spring AOP uses proxy based "weaving". This implies that self calls can't be instrumented (f.i. this.someMethod()) as the real is referenced, not the proxy

## AspectJ integration

To workaround the above limitations, pure AspectJ must be integrated. 

### Dependencies

Just drop aspectjweaver

```
	<dependency>
		<groupId>org.aspectj</groupId>
		<artifactId>aspectjweaver</artifactId>
	</dependency>
```

### Aspect definition

- Aspect are declared using standard AspectJ annotations (@Aspect, @Pointcut, @Around, @Before, etc.)
- A file META-INF/aop.xml must be provided

### Runtime 

A aspectjweaver.{version}.jar must be setup as a vm agent if we want to enable loadtime weaving. Example:

```
    -javaagent:${user.home}/.m2/repository/org/aspectj/aspectjweaver/1.9.4/aspectjweaver-1.9.4.jar
```

Alternatively, we can rely on compile time weaving (through maven-aspectj-plugin if using maven, or the aspectj.gradle plugin is using gradle). 

## Example

This project demonstrates full AspectJ support within a Spring boot application. Methods within the fr.systao..* packages that are annotated with @Profiled are weaved at load time to log the execution time.

 